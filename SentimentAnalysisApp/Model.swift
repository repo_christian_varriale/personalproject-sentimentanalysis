//
//  Model.swift
//  SentimentAnalysisApp
//
//  Created by Christian Varriale on 22/03/2020.
//  Copyright © 2020 Christian Varriale. All rights reserved.
//

import Foundation
import UIKit

struct Model {
    var text: String
    var color: UIColor
    var sentiment: String
    
    init(text: String, color: UIColor, sentiment: String) {
        self.text = text
        self.color = color
        self.sentiment = sentiment
    }
}
